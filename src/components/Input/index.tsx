import ErrorOutlineOutlinedIcon from '@mui/icons-material/ErrorOutlineOutlined';
import { FormControl, InputAdornment, TextField } from '@mui/material';
import React from 'react';

const Input = React.forwardRef(({ type = 'text', label, hint = null, error = '', autoComplete, ...props }: any, ref) => {
  const inputProps = error
    ? {
        endAdornment: (
          <InputAdornment position="end">
            <ErrorOutlineOutlinedIcon />
          </InputAdornment>
        ),
      }
    : {};
  return (
    <FormControl
      fullWidth
      sx={{
        position: 'relative',
        borderRadius: '4px',
        display: 'flex',
        flexDirection: 'column-reverse',
        mb: 6,
      }}
    >
      {hint}
      <TextField
        ref={ref}
        label={label}
        type={type}
        variant="outlined"
        InputLabelProps={{ shrink: true }}
        error={!!error}
        helperText={error}
        InputProps={inputProps}
        autoComplete={autoComplete}
        data-testid={`field${label}`}
        {...props}
      />
    </FormControl>
  );
});

export default Input;
