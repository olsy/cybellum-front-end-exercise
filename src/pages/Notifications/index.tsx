import { Box, Container, Typography } from '@mui/material';
import { useNotificationsLogic } from '../../hooks/useNotifications';

const Notifications = () => {
  const { notifications, isLoading } = useNotificationsLogic();

  let content = null;

  if (isLoading) {
    content = (
      <Typography gutterBottom variant="h1" component="h1">
        Loading...
      </Typography>
    );
  } else if (notifications.length) {
    content = (
      <>
        <Typography gutterBottom variant="h1" component="h1">
          {notifications[0]?.title}
        </Typography>
        <Typography gutterBottom component="p">
          {notifications[0]?.description}
        </Typography>
      </>
    );
  } else {
    content = (
      <Typography gutterBottom variant="h1" component="h1">
        No data
      </Typography>
    );
  }

  return (
    <Container sx={{ display: 'flex', justifyContent: 'center', alignItems: 'center', minHeight: '100vh' }}>
      <Box sx={{ textAlign: 'center' }}>{content}</Box>
    </Container>
  );
};

export default Notifications;
