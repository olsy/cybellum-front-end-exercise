import { Container, Typography } from '@mui/material';

interface Props {
  title: string;
}

const Page = ({ title }: Props) => {
  return (
    <Container>
      <Typography
        gutterBottom
        variant="h1"
        component="h1"
        sx={{
          mt: 4,
          mb: 7,
        }}
      >
        {title}
      </Typography>
      <p>
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam dignissimos eum reiciendis sint. Ab, accusantium doloremque ea impedit itaque laborum
        magni nostrum, odio, pariatur perferendis quaerat quam qui quibusdam quod sunt vel voluptate. Accusantium animi doloremque necessitatibus numquam
        voluptatibus! Aspernatur cumque deleniti ipsam, itaque magnam necessitatibus numquam officia qui quis!
      </p>
      <p>
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aspernatur commodi dolorem ducimus ea expedita libero natus porro quos sapiente similique!
        Blanditiis consequatur dolorem eius eveniet illo iste, minus molestiae neque officia, quis sequi suscipit veritatis! Amet autem dicta doloremque est et,
        explicabo facilis fugiat impedit itaque laboriosam laborum libero modi necessitatibus numquam obcaecati possimus quibusdam, reprehenderit repudiandae
        sint temporibus totam veniam voluptates. Accusamus adipisci, aperiam consectetur cupiditate eveniet incidunt iste laboriosam nesciunt numquam obcaecati
        perspiciatis quam, quidem repellat reprehenderit, suscipit vel veritatis? Accusamus ad asperiores, aspernatur distinctio doloremque doloribus eius
        eveniet expedita iusto molestiae quaerat quis similique, sunt suscipit tempore?
      </p>
      <p>
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aspernatur debitis delectus, deserunt eius error, facere illo ipsam odio officiis, optio
        perferendis quo quod recusandae reprehenderit voluptas. A dicta id incidunt ipsam ipsum modi non possimus quibusdam repellendus sit, tempora voluptatum.
      </p>
    </Container>
  );
};

export default Page;
