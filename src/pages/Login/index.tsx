import { Box, Link, Typography } from '@mui/material';
import { ReactComponent as Logo } from '../../assets/images/cybellum_logo.svg';
import image from '../../assets/images/imac_dig_twins.png';
import LoginForm from '../../forms/LoginForm';
import { Container, IMacImage, IMacImageWrapper } from './styled';

const Login = () => {
  return (
    <>
      <Container>
        <Box
          sx={{
            position: 'relative',
            zIndex: 1,
            '@media (min-width: 1701px)': {
              maxHeight: '50vh',
              transform: 'translate3d(182px, 193px, 0)',
            },
            '@media (min-width: 1571px) and (max-width: 1700px)': {
              transform: 'translate3d(120px, 193px, 0)',
            },
            '@media (min-width: 1490px) and (max-width: 1570px)': {
              transform: 'translate3d(80px, 193px, 0)',
            },
            '@media (min-width: 1231px) and (max-width: 1490px)': {
              transform: 'translate3d(40px, 193px, 0)',
            },
            '@media (max-width: 1231px)': {
              display: 'flex',
              flexDirection: 'column',
              alignItems: 'center',
              justifyContent: 'center',
            },
          }}
        >
          <Logo style={{ display: 'block' }} />
          <Typography
            gutterBottom
            variant="h1"
            component="h1"
            sx={{
              mt: 4,
              mb: 7,
              '@media (min-width: 1231px) and (max-width: 1570px)': {
                fontSize: '48px',
                lineHeight: '54px',
              },
              '@media (max-width: 1230px)': {
                textAlign: 'center',
              },
              '@media (min-width: 200px) and (max-width: 600px)': {
                fontSize: '32px',
                lineHeight: '48px',
              },
            }}
          >
            Welcome to the
            <br />
            Product Security Platform
          </Typography>
          <Box
            sx={{
              width: '400px',
              '@media (max-width: 600px)': {
                width: '100%',
              },
            }}
          >
            <LoginForm />
          </Box>
        </Box>
        <IMacImageWrapper>
          <IMacImage src={image} alt="iMac dig Twins" />
        </IMacImageWrapper>
        <Box
          sx={{
            position: 'absolute',
            bottom: '40px',
            width: '400px',
            '@media (min-width: 1701px)': {
              left: '182px',
            },
            '@media (min-width: 1571px) and (max-width: 1700px)': {
              left: '120px',
            },
            '@media (min-width: 1490px) and (max-width: 1570px)': {
              left: '80px',
            },
            '@media (min-width: 1231px) and (max-width: 1490px)': {
              left: '40px',
            },
            '@media (max-width: 1230px)': {
              top: '40px',
              position: 'relative',
              display: 'flex',
              justifyContent: 'center',
              width: '100%',
            },
          }}
          component="footer"
        >
          <Box
            sx={{
              display: 'flex',
              justifyContent: 'space-between',
              '@media (max-width: 400px)': {
                flexDirection: 'column',
              },
            }}
            component="nav"
          >
            <Link href="/privacy-policy">Privacy policy</Link>
            <Link href="/terms-of-use">Terms of use</Link>
            <Link href="/contact-us">Contact us</Link>
          </Box>
        </Box>
      </Container>
    </>
  );
};

export default Login;
