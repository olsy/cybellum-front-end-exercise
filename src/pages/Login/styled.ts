import { styled } from '@mui/material/styles';

export const Container = styled('div')({
  display: 'flex',
  minHeight: '100vh',
  '@media (max-width: 1230px)': {
    flexDirection: 'column',
    justifyContent: 'center',
    padding: '20px',
  },
});

export const IMacImageWrapper = styled('div')({
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
  position: 'absolute',
  width: 'calc(100vw * 0.636)',
  transform: 'translate3d(calc(100vw * 0.364), 71px, 0)',
  overflow: 'hidden',

  '@media (max-width: 1490px)': {
    width: 'calc(100vw * 0.55)',
    transform: 'translate3d(calc(100vw * 0.45), 193px, 0)',
  },
  '@media (max-width: 1230px)': {
    display: 'none',
  },
});

export const IMacImage = styled('img')({
  position: 'relative',
  width: '100%',
  maxHeight: '943px',
  display: 'block',
  objectFit: 'contain',
});
