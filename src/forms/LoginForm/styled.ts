import { Link } from '@mui/material';
import { styled } from '@mui/material/styles';

export const ForgotPassword = styled(Link)({
  padding: '2px 4px',
  fontSize: '14px',
  fontWeight: 500,
  lineHeight: '20px',
  letterSpacing: '0.1px',
  textAlign: 'left',
  alignSelf: 'flex-start',
});
