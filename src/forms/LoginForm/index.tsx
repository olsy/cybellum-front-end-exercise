import { yupResolver } from '@hookform/resolvers/yup';
import { Button } from '@mui/material';
import { Controller, useForm } from 'react-hook-form';
import * as yup from 'yup';
import Input from '../../components/Input';
import useLogin from '../../hooks/useLogin';
import { ForgotPassword } from './styled';

const schema = yup
  .object({
    email: yup.string().email().required(),
    password: yup.string().min(4).required(),
  })
  .required();
type FormData = yup.InferType<typeof schema>;

const LoginForm = () => {
  const {
    setError,
    control,
    handleSubmit,
    formState: { errors },
  } = useForm<FormData>({
    defaultValues: {
      email: '',
      password: '',
    },
    resolver: yupResolver(schema),
  });

  const { login, isLoading } = useLogin();

  const onSubmit = async (data: any) => {
    try {
      await login(data);
    } catch (error: any) {
      const field = error?.data === 'Incorrect password' ? 'password' : 'email';
      setError(field, { type: error.status, message: error?.data || 'Oh no, there was an error!' });
    }
  };

  return (
    <form onSubmit={handleSubmit(onSubmit)} noValidate>
      <Controller
        name="email"
        control={control}
        render={({ field }) => <Input {...field} type={'email'} autoComplete={'username'} label={'Username'} error={errors.email?.message} />}
      />
      <Controller
        name="password"
        control={control}
        render={({ field }) => (
          <Input
            {...field}
            type={'password'}
            label={'Password'}
            autoComplete={'current-password'}
            hint={<ForgotPassword href="#">Forgot your password?</ForgotPassword>}
            error={errors.password?.message}
          />
        )}
      />
      <Button variant="contained" disableElevation disableRipple fullWidth type="submit" disabled={isLoading} data-testid="submitBtn">
        Log in
      </Button>
    </form>
  );
};

export default LoginForm;
