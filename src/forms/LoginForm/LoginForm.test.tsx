import { fireEvent, screen, waitFor, within } from '@testing-library/react';
import { customRender } from '../../utils/test-utils';
import LoginForm from './index';

describe('<LoginForm />', () => {
  beforeEach(() => {
    customRender(<LoginForm />);
  });

  it('renders form', () => {
    const fieldUsername = screen.getByTestId('fieldUsername').querySelector('input');

    expect(fieldUsername).toBeInTheDocument();

    const fieldPassword = screen.getByTestId('fieldPassword').querySelector('input');

    expect(fieldPassword).toBeInTheDocument();

    const submitBtn = screen.getByTestId('submitBtn');

    expect(within(submitBtn).getByText('Log in')).toBeInTheDocument();
  });

  it('show errors when inputs are empty', async () => {
    const submitBtn = screen.getByTestId('submitBtn');

    fireEvent.click(submitBtn);

    await waitFor(async () => {
      const fieldUsername = screen.getByTestId('fieldUsername').querySelector('p');
      expect(fieldUsername?.textContent).toEqual('email is a required field');
    });

    await waitFor(async () => {
      const fieldPassword = screen.getByTestId('fieldPassword').querySelector('p');
      expect(fieldPassword?.textContent).toEqual('password must be at least 4 characters');
    });
  });

  it('show error invalid email', async () => {
    const submitBtn = screen.getByTestId('submitBtn');

    const fieldUsername = screen.getByTestId('fieldUsername').querySelector('input');

    fieldUsername && fireEvent.change(fieldUsername, { target: { value: 'john@gmail.' } });
    expect(fieldUsername).toHaveValue('john@gmail.');

    fireEvent.click(submitBtn);

    await waitFor(async () => {
      const fieldUsername = screen.getByTestId('fieldUsername').querySelector('p');
      expect(fieldUsername?.textContent).toEqual('email must be a valid email');
    });
  });

  it('submit form with wrong credentials', async () => {
    const submitBtn = screen.getByTestId('submitBtn');

    const fieldUsername = screen.getByTestId('fieldUsername').querySelector('input');

    fieldUsername && fireEvent.change(fieldUsername, { target: { value: 'user@gmail.com' } });

    const fieldPassword = screen.getByTestId('fieldPassword').querySelector('input');

    fieldPassword && fireEvent.change(fieldPassword, { target: { value: 'UserPassword' } });

    fireEvent.click(submitBtn);

    await waitFor(async () => {
      const fieldUsername = screen.getByTestId('fieldUsername').querySelector('p');
      expect(fieldUsername?.textContent).toEqual('Cannot find user');
    });
  });
});
