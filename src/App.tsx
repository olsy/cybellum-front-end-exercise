import { Route, Routes } from 'react-router';
import Login from './pages/Login';
import Notifications from './pages/Notifications';
import Public from './pages/Public';
import { PrivateOutlet } from './utils/PrivateOutlet';

const App = () => {
  return (
      <Routes>
        <Route path="/login" element={<Login />} />
        <Route path="/" element={<PrivateOutlet />}>
          <Route index element={<Notifications />} />
        </Route>
        <Route path="/privacy-policy" element={<Public title="Contact us" />} />
        <Route path="/terms-of-use" element={<Public title="Terms of use" />} />
        <Route path="/contact-us" element={<Public title="Contact us" />} />
      </Routes>
  );
};

export default App;
