import { combineReducers, configureStore } from '@reduxjs/toolkit';
import {authMiddleware, reHydrateAuthData} from '../features/auth/authMiddleware';
import authReducer from '../features/auth/authSlice';
import notificationsSlice from '../features/notifications/notificationsSlice';
import { api } from './services/auth';

const rootReducer = combineReducers({
  [api.reducerPath]: api.reducer,
  auth: authReducer,
  notifications: notificationsSlice,
});

export const store = configureStore({
  reducer: rootReducer,
  preloadedState: {
    auth: reHydrateAuthData()
  },
  devTools: process.env.NODE_ENV !== 'production',
  middleware: (getDefaultMiddleware) => getDefaultMiddleware().concat([api.middleware, authMiddleware]),
});

export default store;
export type RootState = ReturnType<typeof rootReducer>;
export type AppDispatch = typeof store.dispatch;
