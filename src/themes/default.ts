import { createTheme } from '@mui/material/styles';

// A custom theme for this app
const theme = createTheme({
  typography: {
    fontFamily: ['"Noto Sans"', '"Ubuntu"', '"Helvetica Neue"', 'sans-serif'].join(','),
    h1: {
      fontSize: '56px',
      fontWeight: 300,
      lineHeight: '64px',
      letterSpacing: '-0.5px',
    },
  },
  components: {
    MuiCssBaseline: {
      styleOverrides: {
        html: {
          height: '100%',
          scrollBehavior: 'smooth',
        },
        body: {
          WebkitFontSmoothing: 'antialiased',
          MozOsxFontSmoothing: 'grayscale',
          fontFamily: ['"Noto Sans"', '"Ubuntu"', '"Helvetica Neue"', 'sans-serif'].join(','),
          height: '100%',
          overflowY: 'scroll',
        },
        '#root': {
          position: 'relative',
          minHeight: '100vh',
        },
      },
    },
    MuiInputBase: {
      styleOverrides: {
        root: {
          background: '#F9F9FA',
          '& input': {
            padding: '12px 0 12px 16px',
          },
          '&.MuiOutlinedInput-root.Mui-focused .MuiOutlinedInput-notchedOutline': {
            borderWidth: '1px',
          },
          '& .MuiOutlinedInput-notchedOutline': {
            top: 0,
            legend: {
              display: 'none',
            },
          },
          '&.Mui-error .MuiInputAdornment-positionEnd': {
            marginLeft: '4px',
            marginRight: '2px',
            svg: {
              fill: '#BA1A1A',
            },
          },
        },
      },
    },
    MuiInputLabel: {
      styleOverrides: {
        root: {
          left: '16px',
          fontWeight: 400,
          fontSize: '14px',
          lineHeight: '20px',
          letterSpacing: '0.25px',
          color: '#4D4D4D',
          '&.MuiInputLabel-shrink': {
            transform: 'translate(0, -22px)',
          },
        },
      },
    },
    MuiTextField: {
      styleOverrides: {
        root: {
          borderRadius: '4px',
          color: '#1C1B1F',
          borderColor: '#7B7B7B',
          '&:hover': {
            borderColor: '#1C1B1F',
          },
        },
      },
    },
    MuiLink: {
      styleOverrides: {
        root: {
          // Define the default styles for Link
          color: '#4d4d4d',
          // Add more default styles as needed
          // width: Hug (122px)
          // height: Hug (32px)
          padding: '4px 8px',
          borderRadius: '4px',
          //styleName: Cybellum/title/medium - Subtitle2;
          fontSize: '16px',
          fontWeight: 500,
          lineHeight: '24px',
          letterSpacing: '0.15px',
          textAlign: 'center',
          // gap: 4px
          textDecoration: 'none',
          '&:hover': {
            background: '#ebebec',
          },
        },
      },
    },
    MuiButton: {
      styleOverrides: {
        root: {
          height: '40px',
          borderRadius: '4px',
          background: '#BAA182',
          color: '#2A2118',
          fontWeight: 500,
          fontSize: '16px',
          lineHeight: '24px',
          letterSpacing: '0.15px',
          textTransform: 'none',
          '&:hover': {
            background: '#BAA182',
            boxShadow: '0px 1px 3px 1px rgba(28, 27, 31, 0.15), 0px 1px 2px 0px rgba(28, 27, 31, 0.30)',
          },
        },
      },
    },
    MuiFormHelperText: {
      styleOverrides: {
        root: {
          margin: '4px 0 12px',
          fontSize: '12px',
          lineHeight: '16px',
          letterSpacing: '0.4px',
        },
      },
    },
  },
  palette: {
    primary: {
      main: '#46416D',
    },
    secondary: {
      main: '#19857b',
    },
    error: {
      main: '#BA1A1A',
    },
    background: {
      default: '#F9F9FA',
    },
  },
});

export default theme;
