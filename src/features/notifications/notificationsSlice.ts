import type { PayloadAction } from '@reduxjs/toolkit';
import { createSelector, createSlice } from '@reduxjs/toolkit';
import type { RootState } from '../../app/store';

export interface Notification {
  id: number;
  title: string;
  description: string;
  created: string;
}

type NotificationsState = {
  data: Notification[];
};

const slice = createSlice({
  name: 'notifications',
  initialState: { data: [] } as NotificationsState,
  reducers: {
    setNotifications: (state, { payload }: PayloadAction<Notification[]>) => {
      state.data = payload;
    },
  },
});

export const { setNotifications } = slice.actions;

export default slice.reducer;

export const selectBaseNotifications = (state: RootState) => state.notifications.data;
export const selectNotifications = createSelector([selectBaseNotifications], (data) => data);
