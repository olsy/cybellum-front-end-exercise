import { Middleware } from 'redux';
import { RootState } from '../../app/store';
import { setCredentials } from './authSlice';

export const authMiddleware: Middleware<{}, RootState> = (store) => (next) => (action) => {
  if (setCredentials.match(action)) {
    localStorage.setItem('auth', JSON.stringify(action.payload));
  }
  return next(action);
};

export const reHydrateAuthData = () => {
  if (localStorage.getItem('auth') !== null) {
    return JSON.parse(localStorage.getItem('auth') as string);
  } else {
    return { user: null, accessToken: null };
  }
};
