import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useNotificationsMutation } from '../app/services/auth';
import { Notification, selectNotifications, setNotifications } from '../features/notifications/notificationsSlice';

export const useNotifications = () => useSelector(selectNotifications);

export const useNotificationsLogic = () => {
  const dispatch = useDispatch();
  const notifications = useNotifications();

  const [getNotifications, { isLoading }] = useNotificationsMutation();

  useEffect(() => {
    const onMount = async () => {
      const data = await getNotifications().unwrap();
      dispatch(setNotifications(data as unknown as Notification[]));
    };

    onMount().then();
  }, [dispatch, getNotifications]);

  return { notifications, isLoading };
};
