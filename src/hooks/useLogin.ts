import { useDispatch } from 'react-redux';
import { useNavigate } from 'react-router';
import { LoginRequest, useLoginMutation } from '../app/services/auth';
import { setCredentials } from '../features/auth/authSlice';

const useLogin = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const [login, { isLoading }] = useLoginMutation();

  return {
    login: async (data: LoginRequest) => {
      const user = await login(data).unwrap();
      dispatch(setCredentials(user));
      navigate('/');
    },
    isLoading,
  };
};

export default useLogin;
